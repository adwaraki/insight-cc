import json
import line_profiler

class VisaRecord:
    """ A visa processing record """

    def __init__(self, record_values=None, **kwargs):
        """ Create a new visa record

        This visa record is from the LCA Programs 2008 record layout, which acts as the base record class.
        Every year, new record fields are added to a class that is to be subclassed from this base record class. The 2008
        record structure serves as our base and every year, fields are added. For the purposes of this coding challenge,
        we demonstrate 2008 and then 2014-2016 (data presented as a CSV by the coding challenge).

        The record structure is as follows:

        Case_No	- Case number
        Submitted_Date - Date and time the application was submitted
        Program (2007 Only)	- Indicates the type of temporary application submitted for processing.
                              R = H-1B; A = E-3 Australian; C = H-1B1 Chile; S = H-1B1 Singapore
        Employer Name - Employer's name
        Address 1 - Employer's address
        Address2 - Employer's address2
        City - Employer's city
        State - Employer's state
        Postal_Code - Employer's postal code
        Nbr_Immigrants - Number of job openings
        Begin_Date - Proposed begin date
        End_Date - Proposed end date
        Job_Title - Job title
        ......

        For further information on fields, look at
            https://www.foreignlaborcert.doleta.gov/pdf/PerformanceData/2017/H-1B_FY08_Record_Layout.pdf

         """

        self._record_fields = record_values

        # This section is a hack to standardize the column headers that we need. A more efficient way to do this would
        # be to pre-process the dataset beforehand and change all the column names to standard values. Since that is
        # unclear here, we do this for the time being.

        if 'CASE_NO' in self._record_fields.keys():
            self._record_fields['CASE_NUMBER'] = self._record_fields.pop('CASE_NO')
        elif 'LCA_CASE_NUMBER' in self._record_fields.keys():
            self._record_fields['CASE_NUMBER'] = self._record_fields.pop('LCA_CASE_NUMBER')
        elif 'LCA_CASE_NUM' in self._record_fields.keys():
            self._record_fields['CASE_NUMBER'] = self._record_fields.pop('LCA_CASE_NUM')
        elif 'LCA_CASE_NO' in self._record_fields.keys():
            self._record_fields['CASE_NUMBER'] = self._record_fields.pop('LCA_CASE_NO')
        else:
            # We have nothing to do here for now, pass.
            pass

        # We standardize the lca_case_soc_name column name to SOC_NAME and reject all other notations
        if 'LCA_CASE_SOC_NAME' in self._record_fields.keys():
            self._record_fields['SOC_NAME'] = self._record_fields.pop('LCA_CASE_SOC_NAME')

        # We standardize the lca_case_employer_state column name to EMPLOYER_STATE and reject all other notations
        if 'LCA_CASE_EMPLOYER_STATE' in self._record_fields.keys():
            self._record_fields['EMPLOYER_STATE'] = self._record_fields.pop('LCA_CASE_EMPLOYER_STATE')

        # We standardize the status column name to CASE_STATUS and reject all other notations
        if 'STATUS' in self._record_fields.keys():
            self._record_fields['CASE_STATUS'] = self._record_fields.pop('STATUS')

        self._case_number = self._record_fields['CASE_NUMBER']
        # Discard the serial number since we don't need it
        self._record_fields.pop('SERIAL_NUMBER')

        # TODO - The dates can be stored as Unix epoch timestamps. Static helper method added. Complete later, if reqd.

    def __hash__(self):
        """
        Hash functionality for the record class. We hash some of the most unique columns so minimize collisions
        :return: hash of the record
        """
        return hash(''.join(self._record_fields.values()))

    def __eq__(self, other):
        """
        Checks equality between two record objects. This method is just a fallback, because the __hash__ method
        should detect equality for two records, since both of them will return the same hash if they are similar.
        :param other: record to be compared
        :return: True is they are the same, False otherwise
        """
        if not isinstance(other, type(self)):
            return NotImplemented
        return hash(self._record_fields) == hash(other.record_fields)

    def __str__(self):
        """
        Human-readable stringification of object
        :return: Human-readable format of the record class
        """
        return str(json.dumps(self._record_fields))

    # This section contains getters only for those attributes that are deemed unchangeable. Others have setters below
    @property
    def case_number(self):
        return self._case_number

    @property
    def record_fields(self, field=None):
        try:
            if field is None:
                return self._record_fields
            else:
                return self._record_fields[field]
        except KeyError:
            print("Field not present in record. Invalid query.")
            return None

    @property
    def field_header(self):
        return self._field_header

    @field_header.setter
    def field_header(self, header_list):
        # Have to send the entire header list, instead of checking and adjusting each element in the
        # record field list. Easier to just replace the entire thing.
        if type(header_list) is not list:
            print("Need entire record header to update.")
            return False
        else:
            self._field_header = header_list

    @record_fields.setter
    def record_fields(self, field, value):
        try:
            self.record_fields[field] = value
        except KeyError:
            print("Field does not exist in record. Invalid key.")

    def field_in_record(self, field=None):
        return field in self._record_fields

