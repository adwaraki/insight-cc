import line_profiler
import cProfile
from collections import defaultdict
from src.classes.datastore import DataStore

cp = cProfile.Profile()


class HashTableDataStore(DataStore):
    """
    A simple, NO-SQL concept based datastore, backed by a hash-table and supported by simple indexes for
    searching and querying.
    """

    def __init__(self):
        # This is the primary datastore that holds all the records
        self._primary_record_store = {}
        # This is a nested dict that holds the defined indexes to the datastore
        self._index_store = defaultdict(lambda: defaultdict(set))
        # This is a set that holds information about all the column names present across all record types in the
        # datastore. Used to validate a column name when adding an index.
        self._columns = set()

    def __len__(self):
        return len(self._primary_record_store)

    # @profile
    def insert_record(self, primary_key='ABCD1234', record=None):
        """
        This method inserts a record into the datastore
        :param primary_key: The key, or the case number by which the record is identified
        :param record: A visa record of the Record class
        :return: True is record was successfully inserted, False otherwise
        """
        try:
            self._primary_record_store[primary_key] = record
        except KeyError:
            # The only error that can be considered here is a non-hashable entry, which the case number is not.
            print("Error inserting record into datastore.")
            return False

        # Add every field present into the column name set. If already present, no changes, if not, it is added.
        # for key in record.record_fields.keys():
        #     self._columns.add(key)
        self._columns.update(record.record_fields.keys())

        # Update any existing indexes with the new record
        for index_name in self._index_store:
            record_fields = record.record_fields
            if index_name in record_fields:
                self._index_store[index_name][record_fields[index_name]].add(record)

        return True

    def retrieve_record(self, case_number=None):
        """
        Method to retrieve a record from the datastore
        :param case_number: Case number of the record to retrieve
        :return: Retrieved record if successful, False otherwise
        """
        try:
            return self._primary_record_store[case_number]
        except KeyError:
            print("Did not find case number in datastore. Re-check case number")
            return False

    # @profile
    def create_datastore_index(self, column_name=None):
        """
        This method creates a hash-table backed index for the datastore, so that certain columns can
        be queried faster.

        The time-consuming operation is a O(N), where N is the size of the record set only when the index is created,
        but subsequent accesses for the index are all O(1). We can safely say that the amortized time complexity for
        this operation is a O(1).

        :param column_name: the column name on which to create the index
        :return: True is index created successfully, False otherwise
        """
        if (column_name is None) or (column_name not in self._columns):
            raise ValueError("Incorrect column name specified for index creation. Column does not exist.")
        else:
            # Since the index store runs on defaultdict, we don't need to check if the index exists. We just add the
            # case number to the correct index.
            for value in self._primary_record_store.values():
                if value.field_in_record(field=column_name):
                    # We reference the entire record here. If Python did not do references, then this would be
                    # duplicating information, but since it is storing object references here, we can safely add
                    # the information to the set. __hash__ and __eq__ have already been implemented for the
                    # VisaRecord class
                    # cp.enable()
                    self._index_store[column_name][value.record_fields[column_name]].add(value)
                    # cp.disable()
            # cp.print_stats()

        return len(self._index_store)

    def delete_datastore_index(self, column_name=None):
        """
        Delete an index from the index store. This operation will delete all records indexed at that point by that
        index and cannot be undone.

        :param column_name: index on column to be removed
        :return: True on successful removal, False otherwise
        """

        if (column_name is None) or (column_name not in self._columns):
            raise ValueError("Incorrect column name specified for index creation. Column does not exist.")
        else:
            del(self._index_store[column_name])
            return True

    def retrieve_datastore_index(self, column_name=None):
        """
        Retrieves the datastore index to work with, print and analyze
        :param column_name: name of the index to be retrieved
        :return: the entire nested dict associated with the column name
        """

        if (column_name is None) or (column_name not in self._columns):
            raise ValueError("Incorrect column name specified for index creation. Column does not exist.")
        else:
            return self._index_store[column_name]

    def _print_datastore_indexes(self):
        for key, value in self._index_store.items():
            print(key, value)

    def active_index_count(self):
        return len(self._index_store)

    def _replace_datastore(self, datastore_dict=None):
        self._primary_record_store = datastore_dict