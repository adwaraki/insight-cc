from abc import ABC, abstractmethod


class DataStore(ABC):
    """
    Abstract base class representing a datastore, This can be used to subclass any type of datastore
    """

    @abstractmethod
    def insert_record(self, primary_key, record):
        pass

    @abstractmethod
    def retrieve_record(self, case_number):
        pass

    @abstractmethod
    def create_datastore_index(self, column_name):
        pass

    @abstractmethod
    def delete_datastore_index(self, column_name):
        pass

    @abstractmethod
    def retrieve_datastore_index(self, column_name):
        pass