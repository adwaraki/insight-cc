import mmap
import csv
from datetime import datetime


def convert_date_to_epoch(date):
    """
    A utility method that converts a regular date into a Unix epoch

    :param date: The date containing the year, month and day
    :return: A Unix epoch timestamp
    """

    # Ignoring the timezone here for the coding challenge. It can be easily incorporated to account for the slight
    # variations in datetime
    std_date = datetime.strptime(date, '%m/%d/%y')
    return std_date.timestamp()


# It is quite evident that using mmap to read a file is going to be faster than using a lazy generator. The credits
# for this mmap approach go to:
# https://stackoverflow.com/questions/38217514/how-to-open-a-csv-file-for-reading-purpose-using-mmap-in-python
def mmap_csv_file(filename, skip_header=False, transform=lambda row: row, key_selector=lambda o: o):
    """
    Takes a CSV file path and uses mmap to open the file and return a dictionary of the contents keyed
    on the results of the key_selector.  The default key is the transformed object itself.  Mmap is used because it is
    a more efficient way to process large files.

    The transform method is used to convert the line (converted into a list) into something else.  Hence 'transform'.
    If you don't pass it in, the transform returns the list itself.

    The other place where it will save a lot of time is using the transform lambda directly to associate a row with
    a VisaRecord class. It was attempted, but needs some more tweaking to work efficiently. For future work.
    """
    contents = {}
    header = []
    first_line = False

    try:
        with open(filename, "r+b") as f:
            # memory-map the file, size 0 means whole file
            mm = mmap.mmap(f.fileno(), 0)
            for line in iter(mm.readline, b''):
                line = line.decode('utf-8')
                line = line.strip()
                if not first_line:
                    first_line = True
                    if skip_header:
                        header = None
                        continue
                    else:
                        header = next(csv.reader([line], delimiter=';'), '')
                        if header[0] == '':
                            header[0] = 'SERIAL_NUMBER'
                row = next(csv.reader([line], delimiter=';'), '')
                if transform is None and callable(transform):
                    if row is None or row == '':
                        continue
                    value = transform(row)
                else:
                    value = row

                if callable(key_selector):
                    key = key_selector(value)
                else:
                    key = key_selector

                contents[key] = value

    except IOError as ie:
        print('Error decomposing line: {0}'.format(ie))
        return {}
    except:
        raise

    return header, contents

