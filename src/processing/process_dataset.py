import csv
import sys
import time
import line_profiler
from src.classes.record import VisaRecord
from src.utils.utility_methods import mmap_csv_file
from src.classes.hashdatastore import HashTableDataStore

TEST_DATASET = "../../input/h1b_input.csv"
TOP_OCCUPATIONS = "../../output/top_10_occupations.txt"
TOP_STATES = "../../output/top_10_states.txt"

# Create the datastore
H1B_MASTER_DATASTORE = HashTableDataStore()

# @profile
def read_dataset(filename=None):

    start_time = time.time()
    header, mmapped_records = mmap_csv_file(filename, key_selector=lambda row: row[1])
    end_time = time.time()

    print("Time taken to mmap records (in secs): " + str(round(end_time - start_time, 2)))

    # for key, value in mmapped_records.items():
    #     print(key, value)

    for key, value in mmapped_records.items():
        # build kwargs for the constructor
        record_fields = dict(zip(header, value))
        record = VisaRecord(record_values=record_fields)
        record.field_header = header
        H1B_MASTER_DATASTORE.insert_record(record.case_number, record)


def initialize_indexes():

    # Create an index on the required columns, in our case:
    # 1. Index on SOC_NAME for the first result
    # 2. Index on EMPLOYER_STATE for the second result
    # 3. Create an extra index on CASE_STATUS
    # Retrieving the individual indexes will give us the dict that contains information keyed by the
    # respective entries.
    # For example, the SOC_NAME index will be keyed with the different employment titles.
    # Since all the index results are sets, we can use set operations, such as an intersection to simulate
    # table filtering, on a very simplistic level.
    # Once the intersections are created, it is just sorting and calculating the percentages to a file.

    start_time = time.time()
    index_count = H1B_MASTER_DATASTORE.create_datastore_index('SOC_NAME')
    if index_count is not None:
        print("Index creation successful. Number of active indexes: " + str(index_count))
    end_time = time.time()

    print("Time taken to create the SOC_NAME index (in secs): " + str(round(end_time - start_time, 2)))

    start_time = time.time()
    index_count = H1B_MASTER_DATASTORE.create_datastore_index('EMPLOYER_STATE')
    if index_count is not None:
        print("Index creation successful. Number of active indexes: " + str(index_count))
    end_time = time.time()

    print("Time taken to create the EMPLOYER_STATE index (in secs): " + str(round(end_time - start_time, 2)))

    start_time = time.time()
    index_count = H1B_MASTER_DATASTORE.create_datastore_index('CASE_STATUS')
    if index_count is not None:
        print("Index creation successful. Number of active indexes: " + str(index_count))
    end_time = time.time()

    print("Time taken to create the CASE_STATUS index (in secs): " + str(round(end_time - start_time, 2)))


if __name__ == "__main__":

    start_time = end_time = 0

    if len(sys.argv) != 4:
        print("Incorrect number of parameters")
        print("Format: python ./src/process_dataset.py <input_dataset> <output_file1> <output_file2>")
        sys.exit(0)

    else:
        input_dataset = sys.argv[1]
        occupations_output = sys.argv[2]
        states_output = sys.argv[3]

        # Read the dataset in. Currently, the with open uses a lazy generator, but need to find a better way to chunk
        # files up and read them faster. This call also populates the datastore with the records.
        start_time = time.time()
        read_dataset(input_dataset)
        end_time = time.time()

        print("Time taken to populate the datastore (in secs): " + str(round(end_time - start_time, 2)))

        # Initialize the three indexes we need for our operations
        initialize_indexes()

        # Retrieve the set of certified applications
        certified_apps = H1B_MASTER_DATASTORE.retrieve_datastore_index('CASE_STATUS')['CERTIFIED']

        start_time = time.time()
        # We first process the top 10 occupations
        all_occupations = H1B_MASTER_DATASTORE.retrieve_datastore_index('SOC_NAME')
        certified_scores = []
        for key, value in all_occupations.items():
            certified_socs = value & certified_apps
            certified_scores.append((key, len(certified_socs)))

        # Calculate the statistics here
        total_certified_apps = 0
        for entry in certified_scores:
            total_certified_apps += entry[1]

        # Sort scores
        sorted_scores = sorted(certified_scores, key=lambda tup: tup[1], reverse=True)
        end_time = time.time()

        print("Time taken to calculate top 10 occupations (in secs): " + str(round(end_time - start_time, 2)))

        # Write output to file
        with open(occupations_output, 'w+') as out_file:
            out_file.write("TOP_OCCUPATIONS;NUMBER_CERTIFIED_APPLICATIONS;PERCENTAGE")
            out_file.write("\n")
            for entry in sorted_scores[0:10]:
                percentage = round(((float(entry[1]) / total_certified_apps) * 100), 2)
                row = entry[0] + ";" + str(entry[1]) + ";" + str(percentage) + "%"
                out_file.write(row)
                out_file.write("\n")

        # Now process the top 10 states
        all_states = H1B_MASTER_DATASTORE.retrieve_datastore_index('EMPLOYER_STATE')
        certified_scores = []
        sorted_scores = []
        for key, value in all_states.items():
            certified_per_state = value & certified_apps
            certified_scores.append((key, len(certified_per_state)))

        # Sort scores
        sorted_scores = sorted(certified_scores, key=lambda tup: tup[1], reverse=True)

        # Write output to file
        with open(states_output, 'w+') as out_file:
            out_file.write("TOP_STATES;NUMBER_CERTIFIED_APPLICATIONS;PERCENTAGE")
            out_file.write("\n")
            for entry in sorted_scores[0:10]:
                percentage = round(((float(entry[1]) / total_certified_apps) * 100), 2)
                row = entry[0] + ";" + str(entry[1]) + ";" + str(percentage) + "%"
                out_file.write(row)
                out_file.write("\n")