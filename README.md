Insight-Coding-Challenge

Problem:

A newspaper editor was researching immigration data trends on H1B(H-1B, H-1B1, E-3) visa application processing over the past years, trying to identify the occupations and states with the most number of approved H1B visas. She has found statistics available from the US Department of Labor and its Office of Foreign Labor Certification Performance Data. But while there are ready-made reports for 2018 and 2017, the site doesn’t have them for past years. As a data engineer, you are asked to create a mechanism to analyze past years data, specificially calculate two metrics: Top 10 Occupations and Top 10 States for certified visa applications. Your code should be modular and reusable for future. If the newspaper gets data for the year 2019 (with the assumption that the necessary data to calculate the metrics are available) and puts it in the input directory, running the run.sh script should produce the results in the output folder without needing to change the code.

Approach:

The current approach taken to solve this problem is thus:

1. Define a reusable modular class for a visa record. This should effectively be designed in such a way that the base Visa Record class contains all the fields from 2008. Every subsequent year's fields can then be subclassed on that base class and added on. Due to the paucity of time, this was not completed in its entirety. There is a single base class that can definitely be extended if required.

2. The next class is a simple datastore that simulates the behavior of a table in an RDBMS. It actually resembles more of a NO-SQL datastore rather than an RDBMS table. Since Python, and especially Python3 heavily uses the "dict" structure, from a design standpoint, it makes sense to extract the performance of the dict rather than an untested data structure. The datastore comprises of a dict of the type {key: Visa Record}, that holds all the records, keyed on case number. This makes retrieval pretty efficient. 

3. The next design consideration is how do you reduce the access and processing time for a large dataset? The way RDBMSs and NOSQL datastores accomplish this is through indexing. Thus, we simulate a simple indexing mechanism here, again using a dict, but this time, it is a multi-level dict and the values are sets. The reasons behind these choices are thus:
    (a) Most of the columns in the dataset are not unique-valued columns. This means that an index needs to be able to sort and access things efficiently and fast despite duplicate entries. 
    (b) Just using a list to store the values that correspond to the index will not suffice, since they do not filter out duplicates. We need to reduce the time complexity of parsing through a large set of resultant data. Thus, the choice of using sets accomplishes the goal of weeding out duplicates, and provides us with a way of performing set operations, such as unions and intersections. These set operations on a very crude scale simulate WHERE clause filtering, which we require to accomplish the task at hand.

4. The advantage of using indexes might not be visible when running the program just once, since indexes take the most time when populating for the first time. They lower the processing time significantly when subsequent accesses are made. You are encouraged to modify the code and time it to test out subsequent accesses.

4. With this simplistic approach defined, we can go about populating the datastore from the dataset and building the indexes.Once the indexes are populated, in our case, 3 of them, namely CASE_STATUS, EMPLOYER_STATE and SOC_NAME, we filter out only the case numbers with a CERTIFIED status from the CASE_STATUS index. The other two resultant data blocks, when individually intersected with this, simulate an SQL filter, such as "SELECT SOC_NAME, COUNT(SOC_NAME) FROM TABLE WHERE CASE_STATUS="Certified" ORDER-BY SOC_NAME". After we obtain the final result-set, it is just statistically calculating the percentages, which is trivial.

Run Instructions:

The instructions have been followed to the point, for the most part. I could somehow not get the test_suite to run when I added my own tests. As a result, I have removed my tests and just left the code as-is, so that it at least passes the initial run. The code may not seem much, but that is because it has been written in a Pythonic fashion and can efficiently do what it is supposed to. Please do let me know if something does not work. 

Sample Run Times:

After populating the datastore with the 2016 dataset:
Time taken to mmap records (in secs): 11.17
Time taken to populate the datastore (in secs): 20.9
Index creation successful. Number of active indexes: 1
Time taken to create the SOC_NAME index (in secs): 1.61
Index creation successful. Number of active indexes: 2
Time taken to create the EMPLOYER_STATE index (in secs): 1.53
Index creation successful. Number of active indexes: 3
Time taken to create the CASE_STATUS index (in secs): 1.51
Time taken to calculate top 10 occupations (in secs): 0.16
[PASS]: test_1
[PASS]: test_1 top_10_states.txt
[Mon Nov  5 20:37:21 EST 2018] 1 of 1 tests passed

Populated the datastore with the 2015 dataset on top of the existing 2016 dataset:

Time taken to calculate top 10 populations without indexing (in secs): 0.2848970890045166

The final number shows that the time is comparable to the first pass, with almost or more than double the number of records. This shows that indexing is working as expected.

Performance Improvements and Potential Pitfalls:

Currently, the __hash__ and __eq__ methods of the VisaRecord class hash the entire record field dictionary. This is problematic for a few reasons. One, hashing that many fields, most of them mutable is extremely expensive. Secondly, the mutability may lead to potential problems, subtle or otherwise in the logic. These need to be replaced by immutable hashes that are invariant over the lifetime of the object. Due to the time constraint, this was not a high priority, since it reflects on performance rather than functionality. To be added later.
