#!/bin/bash
#
# Use this shell script to compile (if necessary) your code and then execute it. Below is an example of what might be found in this file if your program was written in Python
#
python3 ./src/processing/process_dataset.py /Users/Abhishek/Downloads/H1B_FY_2016.csv ./output/top_10_occupations.txt ./output/top_10_states.txt
